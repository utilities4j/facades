package javax.util.facades.lang;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author marcius.brandao
 */
public final class ExceptionFacade {

    private Throwable exp;
    private boolean printStackTrace = true;
    private final List<String> messages = new LinkedList<>();

    static public ExceptionFacade create(Throwable e) {
        return ExceptionFacade.create(e, true);
    }

    static public ExceptionFacade create(Throwable e, boolean printStackTrace) {
        ExceptionFacade emb = new ExceptionFacade(e, printStackTrace);
        emb.process();
        return emb;
    }

    private ExceptionFacade(Throwable e) {
        this.exp = e;
    }

    private ExceptionFacade(Throwable e, boolean printStackTrace) {
        this.exp = e;
        this.printStackTrace = printStackTrace;
    }

    /**
     * Tenta pegar a melhor mensagem de erro. A �ltima ou uma anterior que
     * come�a com ERROR
     *
     * @return
     */
    public String getMessage() {
        String message = "";
        for (String s : messages) {
            message = s;
            if (message.startsWith("ERROR:")) {
                break;
            }
        }
        return message;
    }

    public String getAllMessages() {
        StringBuilder sb = new StringBuilder();
        for (String message : messages) {
            if (sb.length() <= 0) {
                sb.append(message);
            } else {
                sb.append("\n").append(message);
            }
        }
        return sb.toString();
    }

    private void process() {
        if (printStackTrace) {
            exp.printStackTrace(System.err);
        }

        while (exp != null) {
            getMessage(exp);

            Throwable nextException = getNextExpetion(exp);
            while (nextException != null) {
                getMessage(nextException);
                nextException = getNextExpetion(nextException);
            }

            exp = exp.getCause();
        }
    }

    private Throwable getNextExpetion(Throwable exception) {
        for (Method method : exception.getClass().getMethods()) {
            if (method.getName().contains("NextException")) {
                try {
                    Object result = method.invoke(exception);
                    if (result instanceof Throwable) {
                        return (Throwable) result;
                    }
                } catch (Exception ex) {
                    return null;
                }
            }
        }
        return null;
    }

    private void getMessage(Throwable e) {

        /**
         * The getLocalizedMessage() method of Throwable class is used to get a
         * locale-specific description of the Throwable object when an Exception
         * Occurred. It helps us to modify the description of the Throwable
         * object according to the local Specific message. For the subclasses
         * which do not override this method, the default implementation of this
         * method returns the same result as getMessage().
         */
        String s;
        if (e.getLocalizedMessage() != null) {
            s = e.getLocalizedMessage();
        } else if (e.getMessage() != null) {
            s = e.getMessage();
        } else {
            s = e.toString();
        }

        /**
         * trata mensagem padr�o do WebServiceException e suas subclasses
         */
        s = s.replace(PLEASE_SEE_THE_SERVER_LOG, "")
                .replace(CLIENT_RECEIVED_SOAP__FAULT_FROM_SERVER, "")
                .replace(CLIENT_RECEIVED_AN_EXCEPTION_FROM_SERVER, "");

        if (s.contains(EXCEPTION)) {
            s = s.split(EXCEPTION)[1];
        }

        for (String m : s.split("\n")) {
            addMessage(m);
        }
    }

    private static final String CLIENT_RECEIVED_SOAP__FAULT_FROM_SERVER = "Client received SOAP Fault from server:";
    private static final String CLIENT_RECEIVED_AN_EXCEPTION_FROM_SERVER = "Client received an exception from server:";
    private static final String PLEASE_SEE_THE_SERVER_LOG = "Please see the server log to find more detail regarding exact cause of the failure.";
    private static final String EXCEPTION = "Exception: ";

    private void addMessage(String message) {
        messages.add(message);
    }

    @Override
    public String toString() {
        return getMessage();
    }

    /**
     * Defines a custom format for the stack trace as String.
     *
     * @param aThrowable
     * @return
     */
    public static String getSummaryStackTrace(Throwable aThrowable) {
        final StringBuilder summaryStackTrace = new StringBuilder("SummaryStackTrace:");
        summaryStackTrace.append(aThrowable.toString());
        final String NEW_LINE = System.getProperty("line.separator");
        summaryStackTrace.append(NEW_LINE);

        for (StackTraceElement element : aThrowable.getStackTrace()) {
            summaryStackTrace.append(element);
            summaryStackTrace.append(NEW_LINE);
        }

        return summaryStackTrace.toString();
    }

    //todo
//        Throwable th = new Throwable();
//        for (StackTraceElement elem : th.getStackTrace()){
//            System.out.println(elem.getClassName()+"."+elem.getMethodName()+"("+elem.getLineNumber()+")");
//        }
//        System.out.print("get(" + i + ") ");
}
