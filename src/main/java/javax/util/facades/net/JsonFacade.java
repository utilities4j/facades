package javax.util.facades.net;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author marcius.brandao
 */
public class JsonFacade {

    public static void toJsonPretty(JsonObject json) {
        Map<String, Boolean> config = new HashMap<>();
        config.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory jwf = Json.createWriterFactory(config);
        StringWriter sw = new StringWriter();

        try ( JsonWriter jsonWriter = jwf.createWriter(sw)) {
            jsonWriter.writeObject(json);
            System.out.println(sw.toString());
        }
    }

    public static String formatJsonPretty(String jsonString) {
        if (jsonString == null || jsonString.trim().isEmpty()) {
            return jsonString;
        }
        try {
            StringWriter sw = new StringWriter();
            JsonReader jsonReader = Json.createReader(new StringReader(jsonString));
            JsonObject jsonObj = jsonReader.readObject();
            Map<String, Object> map = new HashMap<>();
            map.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory writerFactory = Json.createWriterFactory(map);
            try ( JsonWriter jsonWriter = writerFactory.createWriter(sw)) {
                jsonWriter.writeObject(jsonObj);
            }
            return sw.toString();
        } catch (Exception ex) {
            //System.out.println("Warning: json inv�lid");
            return jsonString;
        }
    }
}
