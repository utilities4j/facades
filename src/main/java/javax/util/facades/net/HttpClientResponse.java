package javax.util.facades.net;

import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author Marcius
 */
public final class HttpClientResponse {

    private int code = -1;
    private String message;
    private String content;       // Conte�do textual (JSON ou outros)
    private byte[] fileContent;   // Conte�do bin�rio (PDF, arquivo, etc.)
    private String contentType;
    private Exception exceptionDetail;

    // Construtor para erro com exce��o
    public HttpClientResponse(Exception exception) {
        this.code = -1;
        this.exceptionDetail = exception;
        this.message = exception.getMessage();
    }

    // Construtor para resposta com c�digo, mensagem e conte�do textual
    public HttpClientResponse(int code, String message, String contentType, String content) {
        this.code = code;
        this.message = message;
        this.content = content;
        this.contentType = contentType;
    }

    // Construtor para resposta com c�digo, mensagem e conte�do bin�rio (arquivo)
    public HttpClientResponse(int code, String message, String contentType, byte[] fileContent) {
        this.code = code;
        this.message = message;
        this.fileContent = fileContent;
        this.contentType = contentType;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        if (message == null && exceptionDetail != null) {
            message = exceptionDetail.getMessage();
        }
        return message;
    }

    public String getContentType() {
        return contentType;
    }

    public String getContent() {
        return content;
    }

    public Exception getExceptionDetail() {
        return exceptionDetail;
    }

    // M�todo para verificar se a resposta foi bem-sucedida
    public boolean success() {
        return code >= 200 && code < 300;
    }

    // M�todo para verificar se o conte�do � um arquivo bin�rio (por exemplo, PDF)
    private boolean isFileResponse() {
        return fileContent != null;
    }

    @Override
    public String toString() {
        if (success()) {
            if (isFileResponse()) {
                return "SUCESS (" + code + ") : File download (size: " + fileContent.length + " bytes)";
            } else {
                return "SUCESS (" + code + ")" + (content != null ? " : " + content : "");
            }
        } else {
            return "HTTP error code : " + code + " - " + (getMessage() != null ? getMessage() : "")
                    + (content != null ? " (" + content + ")" : "");
        }
    }

    // M�todo para obter o conte�do como JsonObject, se for JSON
    public JsonObject getContentAsJsonObject() {
        if (content != null) {
            try (JsonReader jsonReader = Json.createReader(new StringReader(content))) {
                return jsonReader.readObject();
            }
        }
        return null;
    }

    // M�todo para obter o conte�do como JsonArray, se for JSON
    public JsonArray getContentAsJsonArray() {
        if (content != null) {
            try (JsonReader jsonReader = Json.createReader(new StringReader(content))) {
                return jsonReader.readArray();
            }
        }
        return null;
    }

    // M�todo para obter o conte�do como JsonArray, se for JSON
    public byte[] getContentAsBytes() {
        if (fileContent != null) {
            return fileContent;
        } else {
            return content.getBytes();
        }
    }
}
