package javax.util.facades.net;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static javax.util.facades.net.JsonFacade.formatJsonPretty;

/**
 * Classe simples pra acessar um webservice, por exemplo,<br>
 * sem necessidade de bibliotecas de terceiros, apenas Java.<br>
 *
 * Fonte : <br>
 * https://docs.oracle.com/javase/tutorial/networking/urls/index.html
 *
 * @author Marcius
 */
public final class HttpClientFacade {

    private boolean convertToUtf8 = false;
    private boolean contentTypeJson = false;
    protected boolean verbose = false;
    private int connectTimeout = 10000; //10seg
    private int readTimeout = 10000; //10seg

    private final String url;
    private final Map<String, String> requestProperty = new HashMap<>();

    HttpClientResponse response;

    public HttpClientFacade(String url) {
        this.url = url;
        addRequestProperty("Accept-Encoding", "UTF-8");
    }

    public HttpClientFacade addRequestProperty(String key, String value) {
        requestProperty.put(key, value);
        return this;
    }

    public HttpClientFacade setVerbose(boolean verbose) {
        this.verbose = verbose;
        return this;
    }

    public HttpClientFacade setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    public HttpClientFacade setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public HttpClientResponse GET(String url) {
        response = null;
        try {
            return getResponse(getHttpConnection(url, "GET"));
        } catch (IOException ex) {
            response = new HttpClientResponse(ex);
            if (verbose) {
                System.out.println(response);
            }
            return response;
        }
    }

    public HttpClientResponse GET(String url, String... params) {
        String newUrl = pushParamsInUrl(url, params);
        return GET(newUrl);
    }

    private String pushParamsInUrl(String pUrl, String[] params) {
        Map<String, String> map = new LinkedHashMap<>();
        Pattern pattern = Pattern.compile("\\{(.*?)\\}");
        Matcher matcher = pattern.matcher(pUrl);
        int count = 0;
        while (matcher.find()) {
            String new_param = pUrl.substring(matcher.start(), matcher.end());
            map.put(new_param, params[count++]);
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            pUrl = pUrl.replace(entry.getKey(), entry.getValue());
        }
        return pUrl;
    }

    public HttpClientResponse POST(String url, String... params) {
        String newUrl = pushParamsInUrl(url, params);
        String body = params[params.length - 1];
        return executeMethod("POST", newUrl, body);
    }

    public HttpClientResponse PUT(String url, String body) {
        return executeMethod("PUT", url, body);
    }

    public HttpClientResponse PATCH(String url, String body) {
        return executeMethod("PATCH", url, body);
    }

    private HttpClientResponse executeMethod(String verb, String url1, String body) {
        response = null;
        try {
            HttpURLConnection urlConnection = getHttpConnection(url1, verb);

            try (OutputStream os = urlConnection.getOutputStream()) {
                if (convertToUtf8) {
                    os.write(body.getBytes("UTF-8"));
                } else {
                    os.write(body.getBytes());
                }
                os.flush();
                os.close();
            }
            if (verbose) {
                System.out.println("CONTENT " + (convertToUtf8 ? "(UTF-8)" : ""));

                if (contentTypeJson) {
                    System.out.println(formatJsonPretty(body));
                } else {
                    System.out.println(body);
                }
            }
            return getResponse(urlConnection);
        } catch (IOException ex) {
            response = new HttpClientResponse(ex);
            if (verbose) {
                System.out.println(response);
            }
            return response;
        }
    }

    public HttpClientResponse DELETE(String url) {
        response = null;
        try {
            return getResponse(getHttpConnection(url, "DELETE"));
        } catch (IOException ex) {
            response = new HttpClientResponse(ex);
            if (verbose) {
                System.out.println(response);
            }
            return response;
        }
    }

    public HttpClientResponse DELETE(String url, String... params) {
        String newUrl = pushParamsInUrl(url, params);
        return DELETE(newUrl);
    }

    private HttpClientResponse getResponse(HttpURLConnection urlConnection) {
        response = null;
        StringBuilder content = new StringBuilder();

        try {
            urlConnection.connect();
            InputStream is;

            // Verifica se a resposta foi um erro (c�digo >= 300)
            if (urlConnection.getResponseCode() < 200 || urlConnection.getResponseCode() >= 300) {
                is = urlConnection.getErrorStream();
            } else {
                is = urlConnection.getInputStream();
            }

            // Determinar o tipo de conte�do da resposta
            String contentType = urlConnection.getContentType();

            if (is != null) {
                // Se o tipo de conte�do for JSON ou texto, trata como String
                if (contentType != null && (contentType.contains("application/json") || contentType.contains("application/hal+json") || contentType.contains("text"))) {
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"))) {
                        String inputLine;
                        while ((inputLine = br.readLine()) != null) {
                            content.append(inputLine.trim());
                        }
                    }

                    // Cria uma resposta de texto/JSON
                    response = new HttpClientResponse(
                            urlConnection.getResponseCode(),
                            urlConnection.getResponseMessage(),
                            contentType,
                            contentType.contains("json") ? formatJsonPretty(content.toString()) : content.toString());

                } else {
                    // Se for outro tipo (por exemplo, PDF ou outro arquivo bin�rio), trata como arquivo
                    byte[] fileContent;
                    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                        byte[] buffer = new byte[8192];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            baos.write(buffer, 0, bytesRead);
                        }
                        fileContent = baos.toByteArray();
                    }

                    // Cria uma resposta de arquivo bin�rio
                    response = new HttpClientResponse(
                            urlConnection.getResponseCode(),
                            urlConnection.getResponseMessage(),
                            contentType,
                            fileContent);
                }
            }

            if (verbose) {
                System.out.println(response);
            }
            return response;
        } catch (IOException ex) {
            response = new HttpClientResponse(ex);
            if (verbose) {
                System.out.println(response);
            }
            return response;
        }
    }

    /**
     * Returns a url encoded string.
     *
     * @param map
     * @return
     */
    private static String mapToUrlParams(Map<String, String> map) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String key : map.keySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }
            String value = map.get(key);
            try {
                stringBuilder.append((key != null ? URLEncoder.encode(key, "UTF-8") : ""));
                stringBuilder.append("=");
                stringBuilder.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }

        return stringBuilder.toString();
    }

    private HttpURLConnection getHttpConnection(String pUrl, String type) throws MalformedURLException, IOException {
        URL uri = new URL(this.url + pUrl);
        HttpURLConnection con = (HttpURLConnection) uri.openConnection();
        con.setRequestMethod(type); //type: GET, POST, PUT, DELETE, 

        if (type.equals("POST") || type.equals("PUT")) {
            con.setDoOutput(true); //default false. true if the application intends to send (output) a request body
            //con.setDoInput(true); //default true. if the application intends to read data from the URL connection
        } else if (type.equals("DELETE")) {
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        }

        con.setConnectTimeout(connectTimeout);
        con.setReadTimeout(readTimeout);

        for (String key : requestProperty.keySet()) {
            String value = requestProperty.get(key);
            con.setRequestProperty(key, value);

            convertToUtf8 = value.toUpperCase().contains("UTF-8");
            contentTypeJson = value.contains("/json");
            if (verbose) {
                System.out.println(key + " = " + value);
            }
        }

        if (verbose) {
            System.out.println(type + " " + this.url + pUrl);
        }
        return con;
    }

    public HttpClientResponse getResponse() {
        return response;
    }

}
