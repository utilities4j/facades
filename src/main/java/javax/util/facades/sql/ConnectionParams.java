package javax.util.facades.sql;

/**
 * 
 * @author Marcius Brand�o
 * @param <T> 
 * @version 1.0
 * 
 */
public class ConnectionParams<T extends ConnectionParams<T>> {

    private String url;
    private String username;
    private String password;
    private String defaultSchema;
    private String dataSourceName;
    private boolean verbose = false;

    public T setDataSourceName(String dataSourceName) {
        if (dataSourceName != null && !dataSourceName.isEmpty()) {
            if (!dataSourceName.contains("java:comp")) {
                this.dataSourceName = "java:comp/env/jdbc/" + dataSourceName;
            } else {
                this.dataSourceName = dataSourceName;
            }
        }
        return (T) this;
    }

    public String getDataSourceName() {
        return dataSourceName;
    }

    public T setUrl(String url) {
        this.url = url;
        return (T) this;
    }

    public String getUrl() {
        return url;
    }

    public T setVerbose(boolean verbose) {
        this.verbose = verbose;
        return (T) this;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public T setDefaultSchema(String schema) {
        this.defaultSchema = schema;
        return (T) this;
    }

    public String getDefaultSchema() {
        return defaultSchema;
    }

    public T setUsername(String username) {
        this.username = username;
        return (T) this;
    }

    public String getUsername() {
        return username;
    }

    public T setPassword(String password) {
        this.password = password;
        return (T) this;
    }

    public String getPassword() {
        return password;
    }
}
