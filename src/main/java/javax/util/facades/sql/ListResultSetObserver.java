package javax.util.facades.sql;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Marcius
 */
public class ListResultSetObserver implements ResultSetObserver {

    private final List<Map<String, Object>> rows = new LinkedList<>();

    @Override
    public void beforeResultSet(List<String> columnNames) {

    }

    @Override
    public void update(Map row) {
        rows.add(row);
    }

    @Override
    public void afterResultSet() {

    }

    public List<Map<String, Object>> getRows() {
        return rows;
    }
}
