package javax.util.facades.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Marcius Brand�o
 * @param <T>
 * @version 1.0
 *
 */
public class ConnectionFactory<T extends ConnectionFactory<T>> extends ConnectionParams<T> {

    private Connection connection;
    private DataSource dataSource;

    public T build() {
        getConnection();
        closeConnection();
        return (T) this;
    }

    public T setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        return (T) this;
    }

    private DataSource getDataSource() {
        if (dataSource == null && getDataSourceName() != null && !getDataSourceName().isEmpty()) {
            try {
                Context ictx = new InitialContext();
                dataSource = (DataSource) ictx.lookup(getDataSourceName());
            } catch (NamingException ex) {
                throw new RuntimeException(ex);
            }
        }
        return dataSource;
    }

    public synchronized Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                if (getDataSource() != null || getDataSourceName() != null) {
                    connection = getDataSource().getConnection();
                    log(("Connected to " + getDataSource() + "" + getDataSourceName()).replace("null", ""));
                } else {
                    if (getUsername() != null) {
                        connection = DriverManager.getConnection(getUrl(), getUsername(), getPassword());
                    } else {
                        connection = DriverManager.getConnection(getUrl());
                    }
                    log("Connected to " + getUrl());
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return connection;
    }

    public void closeConnection() {
        try {
            if (connection != null && !connection.isClosed() && connection.getAutoCommit()) {
                connection.close();
                connection = null;
                log("Connection closed");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Controle de transa��es simples e aninhadas
     */
    private int transactions = 0;

    public void beginTransaction() {
        try {
            if (getConnection().getAutoCommit()) {
                getConnection().setAutoCommit(false);
            }
            transactions++;
            log("beginTransaction(" + transactions + ")");
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void commit() {
        try {
            if (getConnection().getAutoCommit()) {
                throw new IllegalStateException("Cannot commit without a transaction");
            }
            transactions--;
            if (transactions == 0) {
                log("Commit(" + (transactions + 1) + ")");
                getConnection().commit();
                getConnection().setAutoCommit(true);
                closeConnection();
            } else if (transactions < 0) {
                throw new UnsupportedOperationException();
            } else {
                log("Commit(" + (transactions + 1) + ")");
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void rollback() {
        try {
            getConnection().rollback();
            log("Rollback");
            transactions = 0;
            connection.close();
            log("Connection closed");
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void log(String message) {
        if (isVerbose()) {
            System.out.println("JdbcFacade: " + message);
        }
    }
}
