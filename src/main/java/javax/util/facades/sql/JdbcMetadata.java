package javax.util.facades.sql;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author Marcius Brnd�o
 * @param <T> 
 * @version 1.0
 * 
 */
public class JdbcMetadata<T extends JdbcMetadata<T>> extends ConnectionFactory<T> {

    /**
     * Verfica se uma tabela existe no bd
     *
     * @param tableFullName
     * @return true se existir, false se n�o existir
     * @@code final
     */
    public boolean isTableExist(String tableFullName) {
        String schemaName = extractSchemaName(tableFullName);
        String tableName = extractTableName(tableFullName);

        try (ResultSet tables = getConnection()
                .getMetaData()
                .getTables(null, schemaName, tableName, new String[]{"TABLE"})) {
            return (tables.next());
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Retornas as colunas da primary key da tabela
     *
     * @param fullTableName
     * @return List dos nomes das colunas
     * @@code final
     */
    public List<String> getPrimaryKeys(String fullTableName) {
        String schemaName = extractSchemaName(fullTableName);
        String tableName = extractTableName(fullTableName);

        List<String> fields = new LinkedList<>();

        try (ResultSet primaryKeys = getConnection()
                .getMetaData()
                .getPrimaryKeys(null, schemaName, tableName.toLowerCase())) {

            while (primaryKeys.next()) {
                fields.add(primaryKeys.getString("COLUMN_NAME").toLowerCase());
            }

            return fields;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private String extractTableName(String fullTableName) {
        return fullTableName.contains(".") ? fullTableName.split("\\.")[1] : fullTableName;
    }

    private String extractSchemaName(String fullTableName) {
        return fullTableName.contains(".") ? fullTableName.split("\\.")[0] : null;
    }

    public List<String> getColumns(ResultSet rs) {
        List<String> columns = new ArrayList<>();
        try {
            ResultSetMetaData metadata = rs.getMetaData();

            for (int column = 1; column <= metadata.getColumnCount(); ++column) {
                columns.add(metadata.getColumnLabel(column));
            }

            return columns;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
