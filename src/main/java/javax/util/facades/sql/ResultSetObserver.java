package javax.util.facades.sql;

import java.util.List;
import java.util.Map;

/**
 *
 * @author marcius.brandao
 */
public interface ResultSetObserver {

    void beforeResultSet(List<String> columnNames);

    void update(Map<String, Object> row);

    void afterResultSet();

}
