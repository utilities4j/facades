package javax.util.facades.sql;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;

/**
 *
 * @author marcius.brandao
 * @param <T>
 */
class DataMapperResultSetObserver<T> implements ResultSetObserver {

    private final Class clazz;
    private final List<T> result = new LinkedList<>();
    private List<Field> fields = new LinkedList<>();

    public List<T> getResult() {
        return result;
    }

    public DataMapperResultSetObserver(Class clazz) {
        this.clazz = clazz;
        fields = Arrays.asList(clazz.getDeclaredFields());
        for (Field field : fields) {
            field.setAccessible(true);
        }
    }

    @Override
    public void update(Map<String, Object> row) {
        try {
            T dto = (T) clazz.getConstructor().newInstance();

            for (Field field : fields) {
                String fieldName = field.getName();

                String columnName = null;
                Column col = field.getAnnotation(Column.class);
                if (col != null && row.containsKey(col.name())) {
                    columnName = col.name();
                } else if (row.containsKey(toColumnName(fieldName))) {
                    columnName = toColumnName(fieldName);
                }

                if (columnName != null) {
                    field.set(dto, row.get(columnName));
                }
            }

            result.add(dto);
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException("Problem with data Mapping. See logs.", e);
        }
    }

    protected String toColumnName(String text) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            char currentChar = text.charAt(i);
            if (currentChar == Character.toUpperCase(currentChar)) {
                builder.append('_')
                        .append(Character.toLowerCase(currentChar));
            } else {
                builder.append(currentChar);
            }
        }
        return builder.toString();
    }

    @Override
    public void beforeResultSet(List<String> columnNames) {
        
    }

    @Override
    public void afterResultSet() {
        
    }

}
