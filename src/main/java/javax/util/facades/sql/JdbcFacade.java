package javax.util.facades.sql;

import java.sql.*;
import java.util.*;

/**
 *
 * @author Marcius Brand�o
 * @version 1.0
 *
 */
public class JdbcFacade extends JdbcMetadata<JdbcFacade> {

    private JdbcFacade() {
    }

    public static JdbcFacade builder() {
        return new JdbcFacade();
    }

    public <T> List<T> query(Class<? extends T> clazz, String sql, Object... params) {
        DataMapperResultSetObserver<T> objectMapper = new DataMapperResultSetObserver<>(clazz);
        query(objectMapper, sql, params);
        return objectMapper.getResult();
    }

    public List<Map<String, Object>> query(String sql, Object... params) {
        ListResultSetObserver observer = new ListResultSetObserver();
        query(observer, sql, params);
        return observer.getRows();
    }

    public void query(ResultSetObserver observer, String sql, Object... params) {
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            log("Querying: " + sql);

            for (int i = 0; i < params.length; i++) {
                setParameter(stmt, i + 1, params[i]);
            }

            try (ResultSet resultSet = stmt.executeQuery()) {
                ResultSetMetaData metaData = resultSet.getMetaData();
                int columnCount = metaData.getColumnCount();

                List<String> columnNames = new ArrayList<>(columnCount);
                for (int i = 0; i < columnCount; i++) {
                    String columnName = metaData.getColumnName(i + 1);
                    columnNames.add(columnName);
                }
                observer.beforeResultSet(columnNames);

                while (resultSet.next()) {
                    Map<String, Object> properties = new LinkedHashMap<>();
                    for (int i = 0; i < metaData.getColumnCount(); i++) {
                        properties.put(metaData.getColumnName(i + 1), resultSet.getObject(i + 1));
                    }
                    observer.update(properties);
                }
                observer.afterResultSet();
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Error executing query", ex);
        } finally {
            closeConnection();
        }
    }

    public int execute(String sql, Object... params) {
        int rowsAffected;
        beginTransaction();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            log("Executing: " + sql);

            for (int i = 1; i <= params.length; i++) {
                setParameter(stmt, i, params[i - 1]);
            }

            rowsAffected = stmt.executeUpdate();
            commit();
            return rowsAffected;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } finally {
            closeConnection();
        }
    }

    public int executeBatch(String sql, List<Object[]> batchParams) {
        int rowsAffected = 0;
        beginTransaction();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            log("Executing batch: " + sql);

            for (Object[] params : batchParams) {
                for (int i = 0; i < params.length; i++) {
                    setParameter(stmt, i + 1, params[i]);
                }
                stmt.addBatch();
            }

            int[] aRowsUpdated = stmt.executeBatch();
            for (int rows : aRowsUpdated) {
                rowsAffected += rows;
            }
            commit();
        } catch (SQLException ex) {
            try {
                getConnection().rollback();
            } catch (SQLException rollbackEx) {
                throw new RuntimeException("Error during rollback", rollbackEx);
            }
            throw new RuntimeException("Error executing batch", ex);
        } finally {
            closeConnection();
        }

        return rowsAffected;
    }

    private void log(String message) {
        if (isVerbose()) {
            System.out.println("JdbcFacade: " + message);
        }
    }

    private void setParameter(PreparedStatement stmt, int index, Object value) throws SQLException {
        if (value instanceof Integer) {
            stmt.setInt(index, (Integer) value);
        } else if (value instanceof String) {
            stmt.setString(index, (String) value);
        } else if (value instanceof Double) {
            stmt.setDouble(index, (Double) value);
        } else if (value instanceof Boolean) {
            stmt.setBoolean(index, (Boolean) value);
        } else if (value instanceof java.sql.Date) {
            stmt.setDate(index, (java.sql.Date) value);
        } else {
            stmt.setObject(index, value);
        }
    }
}
