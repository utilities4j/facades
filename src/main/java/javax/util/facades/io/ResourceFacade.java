package javax.util.facades.io;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * get file from classpath, resources folder
 * 
 * @author Marcius
 */
public class ResourceFacade {   

    /**
     * Use:<br>
     * File f = StreamsFacade.getResourceAsFile("resourcename.ext");<br>
     * File f = StreamsFacade.getResourceAsFile("package/resourcename.ext");
     *
     * @param resourceName
     * @return File of resource
     */
    public static File asFile(String resourceName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        URL resource = classLoader.getResource(resourceName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }

    /**
     * Use:<br>
     * byte[] b = StreamsFacade.getResourceAsBytes("resourcename.ext");<br>
     * byte[] b = StreamsFacade.getResourceAsBytes("package/resourcename.ext");
     *
     * @param resourceName
     * @return resource in bytes[]
     */
    public static byte[] asBytes(String resourceName) throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        URL resource = classLoader.getResource(resourceName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return StreamFacade.readBytes(new File(resource.getFile()));
        }
    }

    /**
     * Use:<br>
     * String s = StreamsFacade.getResourceAbsolutePath("resource.ext");<br>
     * String s = StreamsFacade.getResourceAbsolutePath("package/resource.ext");
     *
     * @param resourceName
     * @return resource in bytes[]
     */
    public static String getAbsolutePath(String resourceName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        URL resource = classLoader.getResource(resourceName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile()).getAbsolutePath();
        }
    }
}
