package javax.util.facades.io;

import java.io.*;
import java.util.Properties;
import javax.util.facades.lang.ExceptionFacade;

/**
 * Facade de java.util.Properties<br>
 *
 * Trata a maioria das exceptions Se o arquivo de propriedades n�o existir, ser�
 * criado. * Pode criar um arquivo de propriedades tempor�rio <br>
 * se um nome n�o for informado
 *
 * @author Marcius
 */
public class PropertiesFacade {

    private final Properties config = new Properties();
    private final String filename;

    public PropertiesFacade() {
        this.filename = StreamFacade.makeFilenameIntemporaryPathSO(".ini");
        readOrMakePropertieFile();
    }

    public PropertiesFacade(String filename) {
        this.filename = filename;
        readOrMakePropertieFile();
    }

    private void readOrMakePropertieFile() {
        try {
            File file = new File(filename);
            if (!file.exists()) {
                file.createNewFile();
            }
            config.load(new FileInputStream(filename));
        } catch (IOException ex) {
            throw new RuntimeException(ExceptionFacade.create(ex, false).getAllMessages());
        }
    }

    public void setProperty(String key, String value) {
        config.setProperty(key, value);
        try (FileOutputStream out = new FileOutputStream(filename)) {
            config.store(out, null);
        } catch (IOException ex) {
            throw new RuntimeException(ExceptionFacade.create(ex, false).getAllMessages());
        }
    }

    public String getProperty(String key) {
        return config.getProperty(key);
    }

    public String getFilename() {
        return filename;
    }

}
