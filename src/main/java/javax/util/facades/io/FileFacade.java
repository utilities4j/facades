package javax.util.facades.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Indentifica o tipo de arquivo a partir da assinatura (bytes) Baseado em
 * dados/codigos encontrados na internet
 *
 * @author Marcius
 * @version 0.5
 */
public class FileFacade {

    public static List<File> getFilesInSubdirectories(File path) {
        List<File> files = new ArrayList<>();
        recursiveGetFilesInSubdirectories(path, files, null);
        return files;
    }

    public static List<File> getFilesInSubdirectories(File path, String filterExtension) {
        List<File> files = new ArrayList<>();
        recursiveGetFilesInSubdirectories(path, files, filterExtension);
        return files;
    }

    private static void recursiveGetFilesInSubdirectories(File path, List<File> files, String filterExtension) {
        File[] list = path.listFiles();

        for (File file : list) {
            if (file.isDirectory()) {
                recursiveGetFilesInSubdirectories(file, files, filterExtension);
            } else {
                if (filterExtension == null || file.getName().endsWith(filterExtension)) {
                    files.add(file);
                }
            }
        }
    }

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    public static boolean equals(String file1, String file2) {
        File f1 = new File(file1);
        File f2 = new File(file2);
        byte[] bufferFile1 = new byte[1048576];
        byte[] bufferFile2 = new byte[1048576];
        int len;
        int position = 0;
        if (f1.length() == f2.length()) {
            try {
                InputStream inputStreamFile1 = new FileInputStream(f1);
                InputStream inputStreamFile2 = new FileInputStream(f2);
                try {
                    while (inputStreamFile1.read(bufferFile1) >= 0) {
                        len = inputStreamFile2.read(bufferFile2);
                        for (int i = 0; i < len; i++) {
                            position++;
                            if (bufferFile1[i] != bufferFile2[i]) {
                                System.out.println("difen�a na posicao" + position);
                                return false; // tamanho igual e conteudo diferente
                            }
                        }
                    }
                } catch (IOException e) {
                }
            } catch (FileNotFoundException e) {
            }
        } else {
            return false; // tamanho e conteudo diferente
        }
        return true; // arquivos iguais
    }

    // Copiers ------------------------------------------------------------------------------------
    /**
     * Copy file. Any existing file at the destination will be overwritten.
     *
     * @param source The file to read the contents from.
     * @param destination The file to write the contents to.
     * @throws IOException If copying file fails.
     */
    public static void copy(File source, File destination) throws IOException {
        copy(source, destination, true);
    }

    /**
     * Copy file with the option to overwrite any existing file at the
     * destination.
     *
     * @param source The file to read the contents from.
     * @param destination The file to write the contents to.
     * @param overwrite Set whether to overwrite any existing file at the
     * destination.
     * @throws IOException If the destination file already exists while
     * <tt>overwrite</tt> is set to false, or if copying file fails.
     */
    public static void copy(File source, File destination, boolean overwrite) throws IOException {
        if (destination.exists() && !overwrite) {
            throw new IOException(
                    "Copying file " + source.getPath() + " to " + destination.getPath() + " failed."
                    + " The destination file already exists.");
        }

        makedir(destination);
        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            input = new BufferedInputStream(new FileInputStream(source));
            output = new BufferedOutputStream(new FileOutputStream(destination));
            int data;
            while ((data = input.read()) != -1) {
                output.write(data);
            }
        } finally {
            close(input, source);
            close(output, destination);
        }
    }

    // Movers -------------------------------------------------------------------------------------
    /**
     * Move (rename) file. Any existing file at the destination will be
     * overwritten.
     *
     * @param source The file to be moved.
     * @param destination The new destination of the file.
     * @throws IOException If moving file fails.
     */
    public static void move(File source, File destination) throws IOException {
        move(source, destination, true);
    }

    /**
     * Move (rename) file with the option to overwrite any existing file at the
     * destination.
     *
     * @param source The file to be moved.
     * @param destination The new destination of the file.
     * @param overwrite Set whether to overwrite any existing file at the
     * destination.
     * @throws IOException If the destination file already exists while
     * <tt>overwrite</tt> is set to false, or if moving file fails.
     */
    public static void move(File source, File destination, boolean overwrite) throws IOException {
        if (destination.exists()) {
            if (overwrite) {
                destination.delete();
            } else {
                throw new IOException(
                        "Moving file " + source.getPath() + " to " + destination.getPath() + " failed."
                        + " The destination file already exists.");
            }
        }

        makedir(destination);

        if (!source.renameTo(destination)) {
            throw new IOException(
                    "Moving file " + source.getPath() + " to " + destination.getPath() + " failed.");
        }
    }

    /**
     * Check and create missing parent directories for the given file.
     *
     * @param file The file to check and create the missing parent directories
     * for.
     * @throws IOException If the given file is actually not a file or if
     * creating parent directories fails.
     */
    private static void makedir(File file) throws IOException {
        if (file.exists() && !file.isFile()) {
            throw new IOException("File " + file.getPath() + " is actually not a file.");
        }
        File parentFile = file.getParentFile();
        if (!parentFile.exists() && !parentFile.mkdirs()) {
            throw new IOException("Creating directories " + parentFile.getPath() + " failed.");
        }
    }

    /**
     * Close the given I/O resource of the given file.
     *
     * @param resource The I/O resource to be closed.
     * @param file The I/O resource's subject.
     */
    private static void close(Closeable resource, File file) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                String message = "Closing file " + file.getPath() + " failed.";
                // Do your thing with the exception and the message. Print it, log it or mail it.
                System.err.println(message);
                e.printStackTrace();
            }
        }
    }
}
