package javax.util.facades.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Useful file system level utilities.
 *
 * @author BalusC
 * @link http://balusc.blogspot.com/2007/09/FileUtil.html
 */
public final class StreamFacade {

    //<editor-fold defaultstate="collapsed" desc="Constants">
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private static final String[] ENCODINGS = {"ISO-8859-1", "UTF-8", "UTF-16"};
    private static final byte GIF_87a[] = new byte[]{(byte) 0x47, (byte) 0x49, (byte) 0x46, (byte) 0x38, (byte) 0x37, (byte) 0x61}; // "GIF87a"
    private static final byte GIF_89a[] = new byte[]{(byte) 0x47, (byte) 0x49, (byte) 0x46, (byte) 0x38, (byte) 0x39, (byte) 0x61}; // "GIF89a"
    private static final byte JPEG[] = new byte[]{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF, (byte) 0xE0};
    private static final byte PNG[] = new byte[]{(byte) 0x89, (byte) 0x50, (byte) 0x4E, (byte) 0x47, (byte) 0x0D, (byte) 0x0A, (byte) 0x1A, (byte) 0x0A};
    private static final byte BMP[] = new byte[]{(byte) 0x42, (byte) 0x4D};
    public static final String CONTENT_TYPE_PLAIN = "text/plain";
    public static final String CONTENT_TYPE_GIF = "image/gif";  // "GIF87a", "GIF89a"
    public static final String CONTENT_TYPE_JPEG = "image/jpeg"; // 0xFF,0xD8,0xFF,0xE0
    public static final String CONTENT_TYPE_PNG = "image/png";  // 0x89,0x50,0x4E,0x47,0x0D,0x0A,0x1A,0x0A
    public static final String CONTENT_TYPE_BMP = "image/bmp";  // 0x89,0x50,0x4E,0x47,0x0D,0x0A,0x1A,0x0A
    public static final String CONTENT_TYPE_OCTET = "application/octet-stream";
    public static final String CONTENT_TYPE_CSV = "text/comma-separated-values";
    public static final String CONTENT_TYPE_CSV2 = "text/csv";
    //</editor-fold>

    private StreamFacade() {
    }

    public static String getMime(byte[] content) {
        String mime = "application/octet-stream";

        if (isJPEG(content)) {
            mime = "image/jpeg";
        } else if (isBMP(content)) {
            mime = "image/bmp";
        } else if (isGIF(content)) {
            mime = "image/gif";
        } else if (isPNG(content)) {
            mime = "image/png";
        } else if (isPDF(content)) {
            mime = "application/pdf";
        } else if (isZIP(content)) {
            mime = "application/zip";
        }

        return mime;
    }

    public static String getExtension(byte[] data) {
        String ext = "";
        try {
            if (isJPEG(data)) {
                ext = ".jpeg";
            } else if (isBMP(data)) {
                ext = ".bmp";
            } else if (isGIF(data)) {
                ext = ".gif";
            } else if (isPNG(data)) {
                ext = ".png";
            } else if (isPDF(data)) {
                ext = ".pdf";
            } else if (isZIP(data)) {
                ext = ".zip";
            } else if (isXML(data)) {
                ext = ".xml";
            }
        } catch (IOException ex) {
        }
        return ext;
    }

    public static boolean isJPEG(byte[] content) {
        boolean result = false;

        // Um arquivo jpeg começa com os hexadecimais "0xFF 0xD8"
        if ((content.length >= 2)
                && (content[0] == (byte) 0xFF)
                && (content[1] == (byte) 0xD8)
                && (content[2] == (byte) 0xFF) /*&& (content[3] == (byte) 0xE0)*/) {
            result = true;
        }
        return result;
    }

    public static boolean isBMP(byte[] content) {
        boolean result = false;

        // Um arquivo jpeg começa com os hexadecimais "0xFF 0xD8"
        if ((content.length >= 2)
                && (content[0] == (byte) 0x42)
                && (content[1] == (byte) 0x4D)) {
            result = true;
        }
        return result;
    }

    public static boolean isGIF(byte[] content) {
        boolean result = false;

        if ((content.length >= 6)
                && (((content[0] == (byte) 0x47)
                && (content[1] == (byte) 0x49)
                && (content[2] == (byte) 0x46)
                && (content[3] == (byte) 0x38)
                && (content[4] == (byte) 0x37)
                && (content[5] == (byte) 0x61))
                || ((content[0] == (byte) 0x47)
                && (content[1] == (byte) 0x49)
                && (content[2] == (byte) 0x46)
                && (content[3] == (byte) 0x38)
                && (content[4] == (byte) 0x39)
                && (content[5] == (byte) 0x61)))) {
            result = true;
        }
        return result;
    }

    public static boolean isPNG(byte[] content) {
        boolean result = false;

        // Um arquivo jpeg começa com os hexadecimais "0xFF 0xD8"
        if ((content.length >= 8)
                && (content[0] == (byte) 0x89)
                && (content[1] == (byte) 0x50)
                && (content[2] == (byte) 0x4E)
                && (content[3] == (byte) 0x47)
                && (content[4] == (byte) 0x0D)
                && (content[5] == (byte) 0x0A)
                && (content[6] == (byte) 0x1A)
                && (content[7] == (byte) 0x0A)) {
            result = true;
        }
        return result;
    }

    public static boolean isPDF(byte[] content) {
        boolean result = false;

        // Um arquivo PDF começa com os hexadecimais "25 50 44 46 2D 31 2E" 
        if ((content.length >= 64)
                && (content[0] == (byte) 0x25)
                && (content[1] == (byte) 0x50)
                && (content[2] == (byte) 0x44)
                && (content[3] == (byte) 0x46)
                && (content[4] == (byte) 0x2D)
                && (content[5] == (byte) 0x31)
                && (content[6] == (byte) 0x2E)) {
            result = true;
        }
        return result;
    }

    public static boolean isZIP(byte[] content) {
        boolean result = false;

        // Um arquivo ZIP começa com os HexaDecimais "50 4B 03 04"
        if ((content.length >= 64)
                && (content[0] == (byte) 0x50)
                && (content[1] == (byte) 0x4B)
                && (content[2] == (byte) 0x03)
                && (content[3] == (byte) 0x04)) {
            result = true;
        }
        return result;
    }

    public static boolean isXML(byte[] content) throws IOException {
        boolean result = false;

        /* Um arquivo XML começa com a tag "<?xml" / ?> */
        if (content.length >= 3) {

            for (String currEncoding : ENCODINGS) {
                String temp = new String(content, currEncoding);

                if ((temp.contains("<?xml")) && (temp.indexOf("?>") > 0)) {
                    result = true;
                }
            }
        }
        return result;
    }

    public static boolean isUtf16Text(byte[] content) throws IOException {
        boolean result = false;

        // Um arquivo text UTF-16 começa com os HexaDecimais "FF FF"
        if ((content.length >= 2)
                && (content[0] == (byte) 0xFF)
                && (content[1] == (byte) 0xFF)) {

            int estimate = (content.length - 2) / 3;

            if (count(content, (byte) 0) > estimate) {
                result = true;
            }
        }

        return result;
    }

    public static boolean isEncrypted(byte[] content) throws IOException {
        boolean result = false;

        if ((content.length > 0)
                && //Um arquivo criptografado possui blocos de 8 bytes
                ((content.length % 8) == 0)
                && (!isPDF(content))
                && (!isXML(content))
                && (!isZIP(content))
                && (!isUtf16Text(content))) {
            for (int i = 0; i < content.length; i++) {
                // Possui caracteres de controle?
                if (isAsciiControl((char) content[i])) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    //<editor-fold defaultstate="collapsed" desc="Writers">    
    /**
     * Write byte inputstream to file with option to append to file or not. If
     * not, then any existing file will be overwritten. It's highly recommended
     * to feed the inputstream as BufferedInputStream or ByteArrayInputStream as
     * those are been automatically buffered.
     *
     * @param file The file where the given byte inputstream have to be written
     * to.
     * @param input The byte inputstream which have to be written to the given
     * file.
     * @param append Append to file?
     * @throws IOException If writing file fails.
     */
    public static void writeToFile(InputStream input, File file, boolean append) throws IOException {
        makedir(file);
        BufferedOutputStream output = null;

        try {
            output = new BufferedOutputStream(new FileOutputStream(file, append));
            int data;
            while ((data = input.read()) != -1) {
                output.write(data);
            }
        } finally {
            close(input, file);
            close(output, file);
        }
    }

    /**
     * Write character reader to file with option to append to file or not. If
     * not, then any existing file will be overwritten. It's highly recommended
     * to feed the reader as BufferedReader or CharArrayReader as those are been
     * automatically buffered.
     *
     * @param file The file where the given character reader have to be written
     * to.
     * @param reader The character reader which have to be written to the given
     * file.
     * @param append Append to file?
     * @throws IOException If writing file fails.
     */
    public static void writeToFile(Reader reader, File file, boolean append) throws IOException {
        makedir(file);
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file, append));
            int data;
            while ((data = reader.read()) != -1) {
                writer.write(data);
            }
        } finally {
            close(reader, file);
            close(writer, file);
        }
    }

    /**
     * Write list of String records to file with option to append to file or
     * not. If not, then any existing file will be overwritten.
     *
     * @param file The file where the given character reader have to be written
     * to.
     * @param records The list of String records which have to be written to the
     * given file.
     * @param append Append to file?
     * @throws IOException If writing file fails.
     */
    public static void writeToFile(List<String> records, File file, boolean append) throws IOException {
        makedir(file);
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file, append));
            for (String record : records) {
                writer.write(record);
                writer.write(LINE_SEPARATOR);
            }
        } finally {
            close(writer, file);
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Convenient Writers">
    public static void saveToFile(byte[] bytes, String filename) throws IOException {
        StreamFacade.writeToFile(new ByteArrayInputStream(bytes), new File(filename), false);
    }

    public static void appendToFile(byte[] bytes, String filename) throws IOException {
        StreamFacade.writeToFile(new ByteArrayInputStream(bytes), new File(filename), true);
    }

    /**
     * Write byte array to file. If file already exists, it will be overwritten.
     *
     * @param file The file where the given byte array have to be written to.
     * @param bytes The byte array which have to be written to the given file.
     * @throws IOException If writing file fails.
     */
    public static void saveToFile(byte[] bytes, File file) throws IOException {
        StreamFacade.writeToFile(new ByteArrayInputStream(bytes), file, false);
    }

    /**
     * Write/overwrite byte array to file
     *
     * @param file The file where the given byte array have to be written to.
     * @param bytes The byte array which have to be written to the given file.
     * @throws IOException If writing file fails.
     */
    public static void appendToFile(byte[] bytes, File file) throws IOException {
        StreamFacade.writeToFile(new ByteArrayInputStream(bytes), file, true);
    }

    /**
     * Write byte inputstream to file. If file already exists, it will be
     * overwritten.It's highly recommended to feed the inputstream as
     * BufferedInputStream or ByteArrayInputStream as those are been
     * automatically buffered.
     *
     * @param file The file where the given byte inputstream have to be written
     * to.
     * @param input The byte inputstream which have to be written to the given
     * file.
     * @throws IOException If writing file fails.
     */
    public static void saveToFile(InputStream input, File file) throws IOException {
        StreamFacade.writeToFile(input, file, false);
    }

    public static void appendToFile(InputStream input, File file) throws IOException {
        StreamFacade.writeToFile(input, file, true);
    }

    /**
     * Write character array to file. If file already exists, it will be
     * overwritten.
     *
     * @param file The file where the given character array have to be written
     * to.
     * @param chars The character array which have to be written to the given
     * file.
     * @throws IOException If writing file fails.
     */
    public static void saveToFile(char[] chars, File file) throws IOException {
        StreamFacade.writeToFile(new CharArrayReader(chars), file, false);
    }

    public static void appendToFile(char[] chars, File file) throws IOException {
        StreamFacade.writeToFile(new CharArrayReader(chars), file, true);
    }

    /**
     * Write string value to file. If file already exists, it will be
     * overwritten.
     *
     * @param file The file where the given string value have to be written to.
     * @param string The string value which have to be written to the given
     * file.
     * @throws IOException If writing file fails.
     */
    public static void saveToFile(String string, File file) throws IOException {
        StreamFacade.writeToFile(new CharArrayReader(string.toCharArray()), file, false);
    }

    public static void appendToFile(String string, File file) throws IOException {
        StreamFacade.writeToFile(new CharArrayReader(string.toCharArray()), file, true);
    }

    /**
     * Write character reader to file. If file already exists, it will be
     * overwritten. It's highly recommended to feed the reader as BufferedReader
     * or CharArrayReader as those are been automatically buffered.
     *
     * @param file The file where the given character reader have to be written
     * to.
     * @param reader The character reader which have to be written to the given
     * file.
     * @throws IOException If writing file fails.
     */
    public static void write(Reader reader, File file) throws IOException {
        StreamFacade.writeToFile(reader, file, false);
    }

    /**
     * Write list of String records to file. If file already exists, it will be
     * overwritten.
     *
     * @param file The file where the given character reader have to be written
     * to.
     * @param records The list of String records which have to be written to the
     * given file.
     * @throws IOException If writing file fails.
     */
    public static void saveToFile(List<String> records, File file) throws IOException {
        writeToFile(records, file, false);
    }

    public static void appendToFile(List<String> records, File file) throws IOException {
        writeToFile(records, file, true);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Readers">
    /**
     * Extrai os bytes de um inputStream
     *
     * @param is. InputStream
     * @return btye[]
     * @throws IOException
     */
    public static byte[] readBytes(InputStream is) throws IOException {
        int len;
        int size = 1024;
        byte[] buf;

        if (is instanceof ByteArrayInputStream) {
            size = is.available();
            buf = new byte[size];
            len = is.read(buf, 0, size);
        } else {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            buf = new byte[size];
            while ((len = is.read(buf, 0, size)) != -1) {
                bos.write(buf, 0, len);
            }
            buf = bos.toByteArray();
        }
        return buf;
    }

    /**
     * Read byte array from file. Take care with big files, this would be memory
     * hogging, rather use readStream() instead.
     *
     * @param file The file to read the byte array from.
     * @return The byte array with the file contents.
     * @throws IOException If reading file fails.
     */
    public static byte[] readBytes(File file) throws IOException {
        BufferedInputStream stream = (BufferedInputStream) readStream(file);
        byte[] bytes = new byte[stream.available()];
        stream.read(bytes);
        return bytes;
    }

    /**
     * Read byte stream from file.
     *
     * @param file The file to read the byte stream from.
     * @return The byte stream with the file contents (actually:
     * BufferedInputStream).
     * @throws IOException If reading file fails.
     */
    public static InputStream readStream(File file) throws IOException {
        return new BufferedInputStream(new FileInputStream(file));
    }

    /**
     * Read character array from file. Take care with big files, this would be
     * memory hogging, rather use readReader() instead.
     *
     * @param file The file to read the character array from.
     * @return The character array with the file contents.
     * @throws IOException If reading file fails.
     */
    public static char[] readChars(File file) throws IOException {
        BufferedReader reader = (BufferedReader) readReader(file);
        char[] chars = new char[(int) file.length()];
        reader.read(chars);
        return chars;
    }

    /**
     * Read string value from file. Take care with big files, this would be
     * memory hogging, rather use readReader() instead.
     *
     * @param file The file to read the string value from.
     * @return The string value with the file contents.
     * @throws IOException If reading file fails.
     */
    public static String readString(File file) throws IOException {
        return new String(readChars(file));
    }

    /**
     * Read character reader from file.
     *
     * @param file The file to read the character reader from.
     * @return The character reader with the file contents (actually:
     * BufferedReader).
     * @throws IOException If reading file fails.
     */
    public static Reader readReader(File file) throws IOException {
        return new BufferedReader(new FileReader(file));
    }

    /**
     * Read list of String records from file.
     *
     * @param file The file to read the character writer from.
     * @return A list of String records which represents lines of the file
     * contents.
     * @throws IOException If reading file fails.
     */
    public static List<String> readRecords(File file) throws IOException {
        BufferedReader reader = (BufferedReader) readReader(file);
        List<String> records = new ArrayList<>();
        String record;

        try {
            while ((record = reader.readLine()) != null) {
                records.add(record);
            }
        } finally {
            close(reader, file);
        }

        return records;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Filenames">
    /**
     * Trim the eventual file path from the given file name. Anything before the
     * last occurred "/" and "\" will be trimmed, including the slash.
     *
     * @param fileName The file name to trim the file path from.
     * @return The file name with the file path trimmed.
     */
    public static String trimFilePath(String fileName) {
        return fileName.substring(fileName.lastIndexOf("/") + 1).substring(fileName.lastIndexOf("\\") + 1);
    }

    /**
     * Generate unique file based on the given path and name. If the file
     * exists, then it will add "[i]" to the file name as long as the file
     * exists. The value of i can be between 0 and 2147483647 (the value of
     * Integer.MAX_VALUE).
     *
     * @param filePath The path of the unique file.
     * @param fileName The name of the unique file.
     * @return The unique file.
     * @throws IOException If unique file cannot be generated, this can be
     * caused if all file names are already in use. You may consider another
     * filename instead.
     */
    public static File uniqueFile(File filePath, String fileName) throws IOException {
        File file = new File(filePath, fileName);

        if (file.exists()) {

            // Split filename and add braces, e.g. "name.ext" --> "name[", "].ext".
            String prefix;
            String suffix;
            int dotIndex = fileName.lastIndexOf(".");

            if (dotIndex > -1) {
                prefix = fileName.substring(0, dotIndex) + "[";
                suffix = "]" + fileName.substring(dotIndex);
            } else {
                prefix = fileName + "[";
                suffix = "]";
            }

            int count = 0;

            // Add counter to filename as long as file exists.
            while (file.exists()) {
                if (count < 0) { // int++ restarts at -2147483648 after 2147483647.
                    throw new IOException("No unique filename available for " + fileName
                            + " in path " + filePath.getPath() + ".");
                }

                // Glue counter between prefix and suffix, e.g. "name[" + count + "].ext".
                file = new File(filePath, prefix + (count++) + suffix);
            }
        }

        return file;
    }

    /**
     * Gera um nome de um arquivo na pasta tempor�ria do SO
     *
     * @param extension
     * @return Nome completo do aquivo
     */
    public static String makeFilenameIntemporaryPathSO(String extension) {
        Integer i = (Calendar.getInstance().get(Calendar.HOUR) * 100000000);
        i += (Calendar.getInstance().get(Calendar.MINUTE) * 1000000);
        i += (Calendar.getInstance().get(Calendar.SECOND) * 10000);
        i += (Calendar.getInstance().get(Calendar.MILLISECOND) * 100);

        return System.getProperty("java.io.tmpdir") + i + (extension == null ? "" : extension);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Check and create missing parent directories for the given file.
     *
     * @param file The file to check and create the missing parent directories
     * for.
     * @throws IOException If the given file is actually not a file or if
     * creating parent directories fails.
     */
    private static void makedir(File file) throws IOException {
        if (file.exists() && !file.isFile()) {
            throw new IOException("File " + file.getPath() + " is actually not a file.");
        }
        File parentFile = file.getParentFile();
        if (!parentFile.exists() && !parentFile.mkdirs()) {
            throw new IOException("Creating directories " + parentFile.getPath() + " failed.");
        }
    }

    /**
     * Close the given I/O resource of the given file.
     *
     * @param resource The I/O resource to be closed.
     * @param file The I/O resource's subject.
     */
    private static void close(Closeable resource, File file) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                String message = "Closing file " + file.getPath() + " failed.";
                // Do your thing with the exception and the message. Print it, log it or mail it.
                System.err.println(message);
            }
        }
    }

    private static boolean isAsciiControl(char ch) {
        if ((ch >= 0x0000) && (ch <= 0x001F)) {
            return true;
        } else {
            return true;
        }
    }

    private static int count(byte[] content, byte value) {
        int result = 0;

        for (int i = 0; i < content.length; i++) {
            if (content[i] == value) {
                result++;
            }
        }

        return result;
    }

    public void write(String contents, String filePath) throws IOException {
        FileWriter fstream = new FileWriter(filePath);
        try (BufferedWriter out = new BufferedWriter(fstream)) {
            out.write(contents);
        }
    }
    //</editor-fold>
}
