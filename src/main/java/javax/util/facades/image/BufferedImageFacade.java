package javax.util.facades.image;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_4BYTE_ABGR;
import java.awt.image.ColorConvertOp;
import java.awt.image.ColorModel;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 *
 * @author Marcius
 */
public class BufferedImageFacade extends Component {

     public enum Align {
        LEFT, CENTER, RIGHT;
    }
     
    private BufferedImage image;

    @Override
    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }

    public BufferedImageFacade(BufferedImage img) {
        this.image = img;
        initFrame("");
    }

    private BufferedImageFacade(String filename) {
        try {
            image = ImageIO.read(new File(filename));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        initFrame(filename);
    }

    private void initFrame(String filename) throws HeadlessException {
        JFrame frame = new JFrame();
        frame.add(this);
        frame.setTitle(filename);
        frame.setResizable(true);
        frame.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void show(String filename) {
        new BufferedImageFacade(filename);
    }

    public static void show(BufferedImage bufferedImage) {
        new BufferedImageFacade(bufferedImage);
    }

    //Green Screen as RGB colour value: 0, 177, 64    
    //Chroma Blue as RGB colour value: 0, 71, 187    
    private static final int THRESHOLD = 177;
    public static final int DEFAULT_IMAGE_TYPE = BufferedImage.TYPE_3BYTE_BGR; //NEW

    public static BufferedImage removeChromaKeyColor(BufferedImage img) {
        //Pega a cor do Chroma Key da Imagem na posi��o 0,0
        Color chromaKeyColor = new Color(img.getRGB(0, 0));
        return removeChromaKeyColor(img, chromaKeyColor);
    }

    public static BufferedImage removeChromaKeyColor(BufferedImage img, Color chromaKeyColor) {
        ImageFilter filter = new RGBImageFilter() {
            // the color we are looking for... Alpha bits are set to opaque
            public int markerRGB = chromaKeyColor.getRGB() | 0xFF000000;
            int r1 = chromaKeyColor.getRed() - THRESHOLD;//(markerRGB & 0xFF0000) >> 16) - THRESHOLD;
            int g1 = chromaKeyColor.getGreen() - THRESHOLD;//((markerRGB & 0xFF00) >> 8) - THRESHOLD;
            int b1 = chromaKeyColor.getBlue() - THRESHOLD;//(markerRGB & 0xFF) - THRESHOLD;

            int r2 = chromaKeyColor.getRed() + THRESHOLD;//((markerRGB & 0xFF0000) >> 16) + THRESHOLD;
            int g2 = chromaKeyColor.getGreen() + THRESHOLD;//((markerRGB & 0xFF00) >> 8) + THRESHOLD;
            int b2 = chromaKeyColor.getBlue() + THRESHOLD;//(markerRGB & 0xFF) + THRESHOLD;

            @Override
            public final int filterRGB(int x, int y, int rgb) {
                int r = (rgb & 0xFF0000) >> 16;
                int g = (rgb & 0xFF00) >> 8;
                int b = rgb & 0xFF;

                if (r >= r1 && r <= r2
                        && g >= g1 && g <= g2
                        && b >= b1 && b <= b2) {//                    
//                 if ((rgb | 0xFF000000) == markerRGB) {
                    // Mark the alpha bits as zero - transparent
                    // Set fully transparent but keep color
                    return 0x00FFFFFF & rgb;
                } else {
                    // nothing to do
                    return rgb;
                }
            }
        };

        ImageProducer ip = new FilteredImageSource(img.getSource(), filter);
        return imageToBufferedImage(Toolkit.getDefaultToolkit().createImage(ip));
    }

    private static BufferedImage imageToBufferedImage(Image image) {
        BufferedImage bufferedImage = new BufferedImage(
                image.getWidth(null),
                image.getHeight(null),
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = bufferedImage.createGraphics();
        g2.drawImage(image, 0, 0, null);
        g2.dispose();

        return bufferedImage;
    }

    public static String saveToPngFile(BufferedImage image, String outputFilename) {
        try {
            if (!outputFilename.endsWith(".png")) {
                outputFilename = outputFilename + System.currentTimeMillis() + ".png";
            }
            ImageIO.write(image, "png", new File(outputFilename));
            return outputFilename;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String saveToFile(BufferedImage image, String outputFilename) {
        try {
            String fileExtension = getFileExtension(outputFilename).toLowerCase();
            ImageIO.write(image, fileExtension, new File(outputFilename));
            return outputFilename;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getFileExtension(String fileName) {
        if (fileName == null || fileName.lastIndexOf('.') == -1) {
            return "";
        }
        return fileName.substring(fileName.lastIndexOf('.') + 1);
    }

    public static BufferedImage printScreen() {
        try {
            Robot robot = new Robot();
            Dimension screenBounds = Toolkit.getDefaultToolkit().getScreenSize();
            Rectangle captureSize = new Rectangle(screenBounds);
            return robot.createScreenCapture(captureSize);
        } catch (AWTException e) {
            throw new RuntimeException(e);
        }
    }

    public static BufferedImage convertToType3ByteBGR(BufferedImage sourceImage) {
        return convertToType(sourceImage, BufferedImage.TYPE_3BYTE_BGR);
    }

    public static BufferedImage convertToType(BufferedImage sourceImage, int type) {
        if (sourceImage.getType() == type) {
            return sourceImage;
        } else {
            BufferedImage image = new BufferedImage(
                    sourceImage.getWidth(),
                    sourceImage.getHeight(),
                    type);
            Graphics2D g2 = image.createGraphics();
            g2.drawImage(sourceImage, 0, 0, null);
            g2.dispose();
            return image;
        }
    }

    //image  = ColorSpaceConvertor.convertColorspace(image, BufferedImage.TYPE_BYTE_GRAY);
    /**
     * convert a BufferedImage to RGB colourspace
     *
     * @param image
     * @param newType
     * @return
     */
    public static BufferedImage convertColorspace(BufferedImage image, int newType) {
        try {
            BufferedImage raw_image = image;
            image = new BufferedImage(
                    raw_image.getWidth(),
                    raw_image.getHeight(),
                    newType);
            ColorConvertOp xformOp = new ColorConvertOp(null);
            xformOp.filter(raw_image, image);
        } catch (Exception e) {
            throw new RuntimeException("Exception " + e + " converting image", e);
        }

        return image;
    }

    public static Graphics2D createGraphics(BufferedImage image) {
        Graphics2D g = image.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        return g;
    }

    public static BufferedImage clone(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    /**
     * Gira o sprite em angle em graus.
     *
     * @param image
     * @param angle
     * @return image rotate em x angle
     */
    static public BufferedImage rotateByDegrees(BufferedImage image, double angle) {
        int w = image.getWidth();
        int h = image.getHeight();
        BufferedImage rotated = new BufferedImage(w, h, image.getType());
        Graphics2D graphic = rotated.createGraphics();
        graphic.rotate(Math.toRadians(angle), w / 2, h / 2);
        graphic.drawImage(image, null, 0, 0);
        graphic.dispose();
        return rotated;
    }

    public static void clearImage(BufferedImage bufferedImage) {
        Graphics2D g = bufferedImage.createGraphics();
        g.setBackground(new Color(0, 0, 0, 0));
        g.clearRect(0, 0,
                bufferedImage.getWidth(),
                bufferedImage.getHeight());
        g.dispose();
    }

    public static BufferedImage loadImage(String filename) {
        try {
            BufferedImage img = ImageIO.read(new File(filename));
            img = BufferedImageFacade.convertToType(img, TYPE_4BYTE_ABGR);
            return img;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) {
        Image resultingImage = originalImage.getScaledInstance(targetWidth, targetHeight, Image.SCALE_DEFAULT);
        BufferedImage outputImage = new BufferedImage(targetWidth, targetHeight, originalImage.getType());
        outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);
        return outputImage;
    }

    public static BufferedImage resizeImageProportional(BufferedImage originalImage, int targetHeight) {
        // Obt�m as dimens�es originais
        int originalWidth = originalImage.getWidth();
        int originalHeight = originalImage.getHeight();

        // Calcula a nova largura proporcional
        int targetWidth = (originalWidth * targetHeight) / originalHeight;

        // Redimensiona a imagem proporcionalmente
        Image resultingImage = originalImage.getScaledInstance(targetWidth, targetHeight, Image.SCALE_SMOOTH);
        BufferedImage outputImage = new BufferedImage(targetWidth, targetHeight, originalImage.getType());
        outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);

        return outputImage;
    }

    public static BufferedImage makeBufferedImage(int width, int heigth) {
        return makeBufferedImage(width, heigth, null);
    }

    public static BufferedImage makeBufferedImage(int width, int heigth, Color color) {
        BufferedImage image = new BufferedImage(width, heigth, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2 = image.createGraphics();

        if (color != null) {
            g2.setColor(color);
            g2.fillRect(0, 0, width, width);
        } else {
            g2.setBackground(new Color(0, 0, 0, 0));
            g2.clearRect(0, 0, image.getWidth(), image.getHeight());
        }
        g2.dispose();
        return image;
    }

    public static void drawString(BufferedImage image, String string, Font font, Color color, Color backgroundColor, Align align) {
        Graphics2D g2d = BufferedImageFacade.createGraphics(image);
        if (backgroundColor != null) {
            g2d.setColor(backgroundColor);
            g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
        }

        g2d.setFont(font);
        g2d.setColor(color);
        FontMetrics fm = g2d.getFontMetrics(font);

        int xPos = 0;
        if (align.equals(Align.CENTER)) {
            xPos = (image.getWidth() - fm.stringWidth(string)) / 2;
        } else if (align.equals(Align.RIGHT)) {
            xPos = image.getWidth() - fm.stringWidth(string);
        }

        int yPos = ((image.getHeight() - fm.getHeight()) / 2) + fm.getAscent();
        g2d.drawString(string, xPos, yPos);
        g2d.dispose();
    }

    public static void drawStringAutosize(BufferedImage image, String string, Font font, Color color, Color backgroundColor, Align align) {
        Graphics2D g2d = BufferedImageFacade.createGraphics(image);
        if (backgroundColor != null) {
            g2d.setColor(backgroundColor);
            g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
        }

        FontMetrics fm = g2d.getFontMetrics();
        double scale = image.getHeight() / (fm.getHeight());
        Font newFont = g2d.getFont().deriveFont(
                font.getStyle(),
                AffineTransform.getScaleInstance(scale, scale));
        g2d.setFont(newFont);
        g2d.setColor(color);
        fm = g2d.getFontMetrics(newFont);

        int xPos = 0;
        if (align.equals(Align.CENTER)) {
            xPos = (image.getWidth() - fm.stringWidth(string)) / 2;
        } else if (align.equals(Align.RIGHT)) {
            xPos = image.getWidth() - fm.stringWidth(string);
        }

        int yPos = ((image.getHeight() - fm.getHeight()) / 2) + fm.getAscent();
        g2d.drawString(string, xPos, yPos);
        g2d.dispose();
    }

    public static void drawStringMultiline(
            BufferedImage image,
            String text,
            Font font,
            Color backgroundColor,
            Align align) {
        Graphics2D g = BufferedImageFacade.createGraphics(image);
        if (backgroundColor != null) {
            g.setBackground(backgroundColor);
            g.fillRect(0, 0, image.getWidth(), image.getHeight());
        }

        int xOffset = 0;
        int yOffset = 0;
        g.setFont(font);
        FontMetrics fm = g.getFontMetrics();
        int leading = (int) (fm.getHeight() * 0.8);//espa�amento entre linhas
        double newY = yOffset + leading;
        if (fm.stringWidth(text) < image.getWidth()) {
            xOffset = align(align, image, fm, text);
            g.drawString(text, xOffset, (int) newY);
        } else {
            String[] words = text.split(" ");
            String currentLine = words[0];
            for (int i = 1; i < words.length; i++) {
                if (fm.stringWidth(currentLine + words[i]) < image.getWidth()) {
                    currentLine += " " + words[i];
                } else {
                    xOffset = align(align, image, fm, currentLine);
                    g.drawString(currentLine, xOffset, (int) newY);
                    newY += leading;
                    currentLine = words[i];
                }
            }
            if (currentLine.trim().length() > 0) {
                xOffset = align(align, image, fm, currentLine);
                g.drawString(currentLine, xOffset, (int) newY);
            }
        }
        g.dispose();
    }

    private static int align(Align align, BufferedImage image1, FontMetrics fm, String text) {
        int xOffset = 0;
        if (align == null || align.equals(Align.LEFT)) {
            xOffset = 0;
        } else if (align.equals(Align.CENTER)) {
            xOffset = (image1.getWidth() - fm.stringWidth(text)) / 2;
        } else if (align.equals(Align.RIGHT)) {
            xOffset = image1.getWidth() - fm.stringWidth(text);
        }
        return xOffset;
    }

    public static void drawImagemInCenter(BufferedImage imageFrom, BufferedImage imagemTarget) {
        Graphics2D g2D = BufferedImageFacade.createGraphics(imagemTarget);

        if (imageFrom.getWidth() < imagemTarget.getWidth()) {
            int xCenter = imagemTarget.getWidth() / 2 - imageFrom.getWidth() / 2;
            g2D.drawImage(imageFrom, xCenter, 0, null);
        } else {
            BufferedImage imageFromResize = resizeImage(imageFrom, imagemTarget.getWidth(), imagemTarget.getHeight());
            g2D.drawImage(imageFromResize, 0, 0, null);
        }

        g2D.dispose();
    }

    // M�todo para concatenar duas BufferedImage horizontalmente
    public static BufferedImage concatenateImages(BufferedImage img1, BufferedImage img2) {
        if (img1 != null && img2 == null) {
            return img1;
        } else if (img1 == null && img2 != null) {
            return img2;
        }

        int width = img1.getWidth() + img2.getWidth();
        int height = Math.max(img1.getHeight(), img2.getHeight());
        BufferedImage concatenatedImage = makeBufferedImage(width, height);
        Graphics2D g2D = BufferedImageFacade.createGraphics(concatenatedImage);
        g2D.drawImage(img1, 0, 0, null);
        g2D.drawImage(img2, img1.getWidth(), 0, null);
        g2D.dispose();
        return concatenatedImage;
    }

    public static int stringWidth(String text, Font font, int width, int heigth) {
        BufferedImage bi = makeBufferedImage(width, heigth);
        Graphics2D g2d = BufferedImageFacade.createGraphics(bi);
        FontMetrics fm = g2d.getFontMetrics();
        double scale = bi.getHeight() / (fm.getHeight());
        Font newFont = g2d.getFont().deriveFont(
                font.getStyle(),
                AffineTransform.getScaleInstance(scale, scale));
        g2d.setFont(newFont);
        int txtWidth = g2d.getFontMetrics(newFont).stringWidth(text);
        g2d.dispose();
        return txtWidth;
    }

    public static int getFontSizeForHeight(Font font, int height) {
        BufferedImage tempImage = new BufferedImage(100, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = tempImage.createGraphics();
        int fontSize = 1;
        Font testFont = font.deriveFont((float) fontSize);
        FontMetrics fm = g2d.getFontMetrics(testFont);
        while (fm.getHeight() < height) {
            fontSize++;
            testFont = font.deriveFont((float) fontSize);
            fm = g2d.getFontMetrics(testFont);
        }
        g2d.dispose();
        return fontSize;
    }

    public static int getHeightForTextWithFont(Font font) {
        BufferedImage tempImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = tempImage.createGraphics();
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        int textHeight = fm.getHeight();
        g2d.dispose();
        return textHeight;
    }

    public static boolean diffImages(BufferedImage bImage, BufferedImage cImage) {
        return diffImages(bImage, cImage, null);
    }

    public static boolean diffImages(BufferedImage bImage, BufferedImage cImage, String saveDiffToFilename) {
        boolean diff = false;
        int height = bImage.getHeight();
        int width = bImage.getWidth();
        BufferedImage rImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                try {
                    int pixelC = cImage.getRGB(x, y);
                    int pixelB = bImage.getRGB(x, y);
                    if (pixelB == pixelC) {
                        rImage.setRGB(x, y, bImage.getRGB(x, y));
                    } else {
                        diff = true;
                        int a = 0xff | bImage.getRGB(x, y) >> 24,
                                r = 0xff & bImage.getRGB(x, y) >> 16,
                                g = 0x00 & bImage.getRGB(x, y) >> 8,
                                b = 0x00 & bImage.getRGB(x, y);

                        int modifiedRGB = a << 24 | r << 16 | g << 8 | b;
                        rImage.setRGB(x, y, modifiedRGB);
                    }
                } catch (Exception e) {
                    // handled hieght or width mismatch
                    rImage.setRGB(x, y, 0x80ff0000);
                }
            }
        }

        if (diff && saveDiffToFilename != null) {
            String filePath = saveDiffToFilename;
            String fileExtenstion = filePath.substring(filePath.lastIndexOf('.'), filePath.length());
            String output = filePath.substring(0, filePath.lastIndexOf('.')) + "-diff" + fileExtenstion;
            saveToPngFile(rImage, output);
        }
        return diff;
    }

    public static void main(String[] args) {
        BufferedImageFacade.show("C:\\Users\\Marcius\\Documents\\NetBeansProjects\\diagramador\\inputfiles\\outros\\kasparov.jpg");
        BufferedImageFacade.show(BufferedImageFacade.printScreen());
        saveToPngFile(printScreen(), "c:\\temp\\diagramador\\printscreen");
    }
}
