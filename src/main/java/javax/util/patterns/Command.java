package javax.util.patterns;

public abstract class Command {

    public enum Chain {

        CONTINUE, STOP;
    }
    public Command chainedCommand;

    public Command getChainedCommand() {
        return chainedCommand;
    }

    public void setChainedCommand(Command chainedCommand) {
        this.chainedCommand = chainedCommand;
    }

    public abstract Chain execute(Object object);

    public void startChain(Object object) {
        if (this.execute(object).equals(Chain.CONTINUE) && chainedCommand != null) {
            chainedCommand.startChain(object);
        }
    }
}
