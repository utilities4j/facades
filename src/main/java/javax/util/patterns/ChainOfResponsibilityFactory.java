package javax.util.patterns;

/**
 * <pre>
 * ChainFactory.createChainActions(
 *                   new StringCommand(),
 *                   new PrimitiveCommand(),
 *                   new EntityCommand(getBackbean()),
 *                   new DownloadCommand(),
 *                   new WorkbookCommand()).
 *                   startChain(result);
 *
 * Command example:
 * public class StringCommand extends Command {
 *   @Override
 *   public Chain execute(Object object) {
 *       if (object instanceof String s) {
 *           //do something 
 *           System.out.println(s); 
 *           return Chain.STOP;
 *       } else {
 *           return Chain.CONTINUE;
 *       }
 *   }
 * }
 * </pre>
 *
 * @author Marcius Brand�o
 */
public class ChainOfResponsibilityFactory {

    public static Command createChainActions(Command... commands) {
        if (commands.length >= 2) {
            for (int i = 1; i < commands.length; i++) {
                commands[i - 1].setChainedCommand(commands[i]);
            }
        }
        return commands[0];
    }
}
