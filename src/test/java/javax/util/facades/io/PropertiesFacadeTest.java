package javax.util.facades.io;

import javax.util.facades.io.PropertiesFacade;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Xadrez
 */
public class PropertiesFacadeTest {
      
    @Test
    public void testPropertiesFacade() throws Exception {
        String key = "key";
        String value = "value";
        String expResult = "value";
        
        PropertiesFacade instance = new PropertiesFacade();
        assertNull(instance.getProperty(key));
        
        instance.setProperty(key, value);
        assertEquals(expResult, instance.getProperty(key));
        
        String filename = instance.getFilename();
        instance = new PropertiesFacade(filename);
        assertEquals(expResult, instance.getProperty(key));
    }
    
}
