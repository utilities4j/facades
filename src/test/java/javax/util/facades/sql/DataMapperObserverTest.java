package javax.util.facades.sql;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marcius.brandao
 */
public class DataMapperObserverTest {

    public DataMapperObserverTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testToColumnName() {
        System.out.println("testToColumnName");
        DataMapperResultSetObserver instance = new DataMapperResultSetObserver(User.class);
        assertEquals("id", instance.toColumnName("id"));
        assertEquals("data_de_nascimento", instance.toColumnName("dataDeNascimento"));
    }

    @Test
    public void testUpdate() throws ParseException {
        System.out.println("testToColumnName");

        DataMapperResultSetObserver instance = new DataMapperResultSetObserver(User.class);

        UUID ID = UUID.fromString("19436713-1187-479a-a94b-0342533f3ca2");
        Date dataDeNascimento = new SimpleDateFormat("dd/MM/yyyy").parse("23/11/2015");
        Map<String, Object> values = new LinkedHashMap<>();
        values.put("id", ID);
        values.put("data_de_nascimento", dataDeNascimento);
        values.put("name", "NOME");

        instance.update(values);
        User user = (User) instance.getResult().get(0);
        System.out.println(user);

        assertEquals(ID, user.getId());
        assertEquals("NOME", user.getNome());
        assertEquals(dataDeNascimento, user.getDataDeNascimento());
        assertEquals(null, user.getIdade());
        assertEquals(null, user.getAltura());
        assertEquals(null, user.getAtivo());
    }

}
