package javax.util.facades.sql;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;

/**
 *
 * @author marcius.brandao
 */
public class User {

    private UUID id;

    @Column(name = "name")
    private String nome;

    private Date dataDeNascimento;

    private Boolean ativo;

    private Integer idade;

    private Float altura;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(Date dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public Float getAltura() {
        return altura;
    }

    public void setAltura(Float altura) {
        this.altura = altura;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "User{"
                + "id=" + id
                + "\n, nome=" + nome
                + "\n, dataDeNascimento=" + dataDeNascimento
                + "\n, ativo=" + ativo
                + "\n, idade=" + idade
                + "\n, altura=" + altura
                + '}';
    }

}
