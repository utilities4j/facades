package javax.util.facades.sql;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marcius.brandao
 */
public class JdbcFacadeTest {

    static public String URL_BD = "jdbc:postgresql://localhost:5499/postgres?user=postgres&password=postgres";
    static private JdbcFacade instance;
    static private final String CREATE_TABLE_USER = ""
            + "CREATE TABLE users ("
            + "	id uuid NOT NULL,"
            + "	account_non_locked bool NULL,"
            + "	cpf varchar(255) NULL,"
            + "	email varchar(255) NULL,"
            + "	enabled bool NULL,"
            + "	name varchar(255) NULL,"
            + "	\"role\" varchar(255) NULL,"
            + "	CONSTRAINT un_email UNIQUE (email),"
            + "	CONSTRAINT users_pkey PRIMARY KEY (id)"
            + ")"
            + "";
    private static final String INSERT_INTO_USERS = "INSERT INTO users(id, cpf, email, enabled, name, role) VALUES(?,?,?,?,?,?)";
    private static final String SELECT_FROM_USERS = "SELECT * FROM USERS";
    private static final String SELECT_COUNT_USERS = "SELECT count(*) FROM USERS";
    private static final String SELECT_FROM_USERS_BY_ID = "SELECT * FROM USERS WHERE ID = ?";
    private static final String DELETE_FROM_USERS = "DELETE FROM USERS";
    private static final String DELETE_FROM_USERS_BY_ID = "DELETE FROM USERS WHERE ID = ?";

    private final Object[] paramsUser1 = {UUID.fromString("a9f31c36-e7de-4806-ac44-0db8062b5679"), "CPF1", "teste1@uece.br", true, "TESTE1", "ROLE_SYSTEM"};
    private final Object[] paramsUser2 = {UUID.fromString("19436713-1187-479a-a94b-0342533f3ca2"), "CPF2", "teste2@uece.br", false, "TESTE2", "ROLE_SYSTEM"};
    private final Object[] paramsUser3 = {UUID.fromString("434c91c0-e277-49e1-8af0-688157218e46"), "CPF3", "teste3@uece.br", true, "TESTE3", "ROLE_SYSTEM"};

    public JdbcFacadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = JdbcFacade.builder().setUrl(URL_BD).setVerbose(true).build();
        if (!instance.isTableExist("users")) {
            instance.execute(CREATE_TABLE_USER);
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.out.println("setUp()");
        instance.execute(DELETE_FROM_USERS);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCRUD() {
        System.out.println("testCRUD()");

        int rows = instance.execute(INSERT_INTO_USERS, paramsUser1);
        assertEquals(1, rows);

        rows = instance.execute(INSERT_INTO_USERS, paramsUser2);
        assertEquals(1, rows);

        List<User> usuarios = instance.query(User.class, SELECT_FROM_USERS);
        assertEquals(2, usuarios.size());

        usuarios = instance.query(User.class, SELECT_FROM_USERS_BY_ID, paramsUser1[0]);
        assertEquals(1, usuarios.size());

        List<Map<String, Object>> tuples = instance.query(SELECT_FROM_USERS);
        assertEquals(2, tuples.size());

        tuples = instance.query(SELECT_FROM_USERS_BY_ID, paramsUser2[0]);
        assertEquals(1, tuples.size());

        rows = instance.execute(DELETE_FROM_USERS);
        assertEquals(2, rows);
    }

    @Test
    public void testRollback() {
        System.out.println("testRollback()");
        instance.beginTransaction();
        try {
            instance.execute(INSERT_INTO_USERS, paramsUser1);
            instance.execute(INSERT_INTO_USERS, paramsUser2);
            instance.execute(INSERT_INTO_USERS);
            instance.commit();
        } catch (Exception ex) {
            instance.rollback();
        }
        assertEquals(0L, instance.query(SELECT_COUNT_USERS).get(0).get("count"));
    }

    @Test
    public void testPrimaryKey() {
        System.out.println("testPrimaryKey()");
        List< String> fields = instance.getPrimaryKeys("users");
        assertEquals(1, fields.size());
        assertEquals("id", fields.get(0));

        instance.getPrimaryKeys("public.users");
        assertEquals(1, fields.size());
        assertEquals("id", fields.get(0));
    }

    @Test
    public void testTransaction() {
        System.out.println("testTransaction()");

        instance.beginTransaction();
        try {
            instance.execute(INSERT_INTO_USERS, paramsUser1);
            instance.execute(INSERT_INTO_USERS, paramsUser2);
            assertEquals(2L, instance.query(SELECT_COUNT_USERS).get(0).get("count"));
            instance.commit();

            instance.execute(DELETE_FROM_USERS_BY_ID, paramsUser1[0]);
            instance.execute(DELETE_FROM_USERS_BY_ID, paramsUser2[0]);
            assertEquals(0L, instance.query(SELECT_COUNT_USERS).get(0).get("count"));
        } catch (Exception ex) {
            instance.rollback();
        }
    }
}
