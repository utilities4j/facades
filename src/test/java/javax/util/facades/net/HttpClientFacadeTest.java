/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javax.util.facades.net;

import javax.util.facades.net.HttpClientFacade;
import javax.util.facades.net.HttpClientResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcius
 */
public class HttpClientFacadeTest {

    public HttpClientFacadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of GET method, of class HttpClientFacade.
     */
    @Test
    public void testGET() throws IOException {
        System.out.println("GET");
        String url = "http://viacep.com.br/ws/61940045/json";
        HttpClientResponse result = new HttpClientFacade("http://viacep.com.br/ws/").GET("61940045/json");
        assertEquals(200, result.getCode());
        assertTrue(result.getContent().contains("61940-045"));

        System.out.println(result.getContent());        

//        InputStream inputStream = new ByteArrayInputStream(result.content.getBytes(Charset.forName("UTF-8")));
//        JsonReader jsonReader = Json.createReader(inputStream);
//        JsonObject jsonObject = jsonReader.readObject();
//        jsonReader.close();
//        inputStream.close();
//        System.out.println("LOGRADOURO"+jsonObject.get("logradouro"));              

    }

    private static final String url = "https://www.google.com/recaptcha/api/";

    //@Test
    public void teste() {

        HttpClientFacade http = new HttpClientFacade(url);
        http.setVerbose(true);
        http.addRequestProperty("Accept-Language", "pt-BR");

        //String postParams = "?secret=" + "6LcsyhgUAAAAAO2rHkrE5ZXW1YQ2D4ZFDrUhImpC" + "&response="+ gRecaptchaResponse;
        String r = "03AGdBq27uyf5P5D_Eg1vwmV7linbTa58Ym6ntmB_d1HypmLBnpKITM5FpWOXzxq3Ghn7ItGQPe22Yjwy-EO0egUGcF2EkKOfxuUy5aQwCFPGTV0cuVMJESWTgYAzMJhIr7_0t9aY9c89S8dAI-qsyWyFPYWC3e0QsRRiTurRxMnM9pb5xKVE-RQKzUxB3xr2kTwu2oei1n_nxq79fSlRMgRcXCP2kFQ1E75-ruuhybcxHukwJd66rYHbPDTGpWu8jaPUNUkgz7W5oGnBgcn4h9RkjSCIXge3K_PLOCvfXeKmCsAv84Tlm__hKaBw8fr1_HF19aXZ-TtIfy350tWbHfPBo-HhXyZO1iwqflczMs0ytZJ1sAHqu9WSYdGvtp8t4k8xROEl-tIPimFowR1vDguRA8yq2m3bJJmHc29aKQyegDu0LxCe36RkKBkyc7rs4DcSDnWP4ziY5";
        HttpClientResponse reponse = http.POST("siteverify?secret=6LcsyhgUAAAAAAXljgSQsVK_0ra-bdG8DmG_sZXq&response=" + r, "");
        //System.out.println(reponse);

    }

}
